export const asyncChannels = Object.freeze({
    httpRequest: 'http-request-async-channel',
    httpRequestReply: 'http-request-async-channel-reply',

    httpGet: 'http-get-async-channel',
    httpGetReply: 'http-get-async-channel-reply',

    httpDelete: 'http-delete-async-channel',
    httpDeleteReply: 'http-delete-async-channel-reply',

    httpHead: 'http-head-async-channel',
    httpHeadReply: 'http-head-async-channel-reply',

    httpPost: 'http-post-async-channel',
    httpPostReply: 'http-post-async-channel-reply',

    httpPut: 'http-put-async-channel',
    httpPutReply: 'http-put-async-channel-reply',

    httpPatch: 'http-patch-async-channel',
    httpPatchReply: 'http-patch-async-channel-reply',

    showOpenDialog: 'show-open-dialog',
    codeGeneratorTemplates: 'code-generator-templates',
    codeGeneratorCheckDb: 'code-generator-check-db',
    codeGeneratorCheckModule: 'code-generator-check-module',
    codeGeneratorRun: 'code-generator-run'
});

export function replyOf(channel: string) {
    return channel + '-reply'
}
