export class DbField {
    readonly name: string
    readonly table: string
    readonly type: string
    readonly length: number
    readonly primary: boolean

    constructor(props: DbField) {
        this.name = props.name
        this.table = props.table
        this.type = props.type
        this.length = props.length
        this.primary = props.primary
    }
}
