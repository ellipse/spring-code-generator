import { DbField } from './db-field'

export interface DbDialect {
    tables(): Promise<string[]>

    tableFields(tableName: string): Promise<DbField[]>
}
