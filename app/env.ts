import * as path from 'path'
import { app } from 'electron'

export const args = process.argv.slice(1)
export const serve = args.some(val => val === '--serve')

export const rootPath = serve ? __dirname : path.dirname(app.getPath('exe'));
