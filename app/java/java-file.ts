import { IFile } from './base'

export const JAVA_PACKAGE_REGEXP = /package (\S+)\s*;/
export const JAVA_CLASSNAME_REGEXP = /(?:public )?(?:abstract )?(?:final |static | )*(?:class|interface) (\S*?)(<.*>)? (?:extends|implements)?.*\{/

export class JavaFile implements IFile {
    readonly path: string
    readonly filename: string
    readonly content: string
    readonly package: string
    readonly className: string

    constructor(props: { path: string, filename: string, package: string, className: string, content: string }) {
        this.path = props.path
        this.filename = props.filename
        this.package = props.package
        this.className = props.className
        this.content = props.content
    }
}

export function parseJavaFile(content: string) {
    const [, pkg] = JAVA_PACKAGE_REGEXP.exec(content) || []
    const [, className] = JAVA_CLASSNAME_REGEXP.exec(content) || []
    return {
        package: pkg,
        className,
        content,
    }
}

async function test() {
    const content = `
package cafebabe.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Misty on 2020-06-03
 */
@Data
@Entity
@Table(name = "cafebabe")
public class CafeBabe {
    @Id
    private Long id;

    /*content*/
}
`
}

// test()
