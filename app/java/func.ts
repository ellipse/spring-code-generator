import { JavaField } from './java-field'

export class Func {
    readonly name: string;
    readonly beanFields: JavaField[] = []

    constructor(props: Func) {
        this.name = props.name
        this.beanFields = props.beanFields
    }
}
