import { IFile } from './base'

export class ResourceFile implements IFile {
    readonly path: string
    readonly filename: string
    readonly content: string

    constructor(props: { path: string, filename: string, content: string }) {
        this.path = props.path
        this.filename = props.filename
        this.content = props.content
    }
}
