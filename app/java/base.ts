export interface IFile {
    readonly path: string
    readonly filename: string
    readonly content: string
}
