// @ts-ignore
import { ModuleConfig } from './module-config';
import { camelcase, capital } from '../util';

export interface BeanStrategy {
    readonly camelName: string
    readonly capitalName: string

    readonly table: string
    readonly package: string
    readonly application: string
    readonly controller: string
    readonly service: string
    readonly dao: string
    readonly vo: string
    readonly entity: string // for jpa
    readonly pojo: string // for mybatis
    readonly property: string
}

export class DefaultBeanStrategy implements BeanStrategy {
    readonly camelName: string
    readonly capitalName: string

    readonly table: string
    readonly package: string
    readonly application: string
    readonly controller: string
    readonly service: string
    readonly dao: string
    readonly vo: string
    readonly entity: string
    readonly pojo: string
    readonly property: string

    constructor(tableName: string, config?: ModuleConfig) {
        this.table = tableName
        let name = tableName;
        if (config && config.tableNamePrefix && tableName.startsWith(config.tableNamePrefix)) {
            name = tableName.slice(config.tableNamePrefix.length, tableName.length);
        }

        const camelName = camelcase(name);
        const capitalName = capital(camelName);

        this.camelName = camelName;
        this.capitalName = capitalName;
        this.package = camelName.toLowerCase()
        this.application = capitalName + 'Application';
        this.controller = capitalName + 'Controller';
        this.service = capitalName + 'Service';
        this.dao = capitalName + 'Dao';
        this.entity = capitalName;
        this.pojo = capitalName;
        this.vo = capitalName + 'Vo';
        this.property = camelName;
    }
}

function test() {
    const st = new DefaultBeanStrategy('hello_world', new ModuleConfig({
        database: '',
        username: '',
        password: '',
        tableNamePrefix: undefined,
    }));

    const ph = new DefaultBeanStrategy('CafeBabe')

    console.log(st)
    console.log(ph)
}

// test();
