export class JavaField {
    readonly isId: boolean
    readonly name: string
    readonly type: string
    readonly simpleTypeName?: string
    readonly needImport?: boolean

    constructor(props: JavaField) {
        this.isId = props.isId
        this.name = props.name
        this.type = props.type
        this.simpleTypeName = this.type

        const idx = this.type.lastIndexOf('.');
        if (idx >= 0) {
            this.simpleTypeName = this.type.slice(idx + 1)
            if (!this.type.startsWith('java.lang')) {
                this.needImport = true
            }
        }
    }
}
