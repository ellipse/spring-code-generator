import { DbConfig } from '../db/db-config'

type Params = Partial<ModuleConfig>

export class ModuleConfig implements DbConfig {
    readonly host?: string
    readonly port?: number
    readonly username?: string
    readonly password?: string
    readonly database: string

    readonly tableNamePrefix?: string
    readonly package?: string
    readonly projectRoot: string
    readonly moduleName: string

    constructor(params: Params) {
        this.host = params.host
        this.port = params.port
        this.username = params.username
        this.password = params.password
        this.database = params.database || ''
        this.tableNamePrefix = params.tableNamePrefix
        this.package = params.package
        this.projectRoot = params.projectRoot || ''
        this.moduleName = params.moduleName || ''
    }
}
