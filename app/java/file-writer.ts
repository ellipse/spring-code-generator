import { listTemplates } from '../template'
import { JavaGenerator } from './java-generator'
import { IFile } from './base'
import { writeFile } from '../util'

export class FileWriter {
    async write(file: IFile) {
        let content = file.content
        if (file.filename.endsWith('.java')) {
            content = `/*
 * Generate by SpringCodeGenerator
 *
 * https://gitee.com/ellipse/spring-code-generator.git
 */
${content}`
        }
        await writeFile(file.path, content)
    }
}

async function test() {
    const generator = new JavaGenerator({
        package: 'org.misty.test',
        tableNamePrefix: 't_',
        password: '123456',
        database: 'spring-code-generator-test',
        projectRoot: 'c:/Users/Misty/Desktop/test',
        moduleName: 'module1'
    });
    const temps = await listTemplates();

    await generator.generate(temps[0])
}

// test()
