import * as path from 'path';
import { rootPath, serve } from './env'

export const loggerPath = serve ? path.join(rootPath, 'electron.log') : path.join(rootPath, 'resources/electron.log')

class Logger {
    private readonly logger;

    constructor() {
        try {
            this.logger = require('electron-log');
            this.logger.transports.file.file = loggerPath
        } catch (e) {
        }
    }

    /**
     * Log an error message
     */
    error(...params: any[]) {
        if (this.logger) {
            this.logger.error(params);
        }
    }

    /**
     * Log a warning message
     */
    warn(...params: any[]) {
        if (this.logger) {
            this.logger.warn(params);
        }
    }

    /**
     * Log an informational message
     */
    info(...params: any[]) {
        if (this.logger) {
            this.logger.info(params);
        }
    }

    /**
     * Log a verbose message
     */
    verbose(...params: any[]) {
        if (this.logger) {
            this.logger.verbose(params);
        }
    }

    /**
     * Log a debug message
     */
    debug(...params: any[]) {
        if (this.logger) {
            this.logger.debug(params);
        }
    }

    /**
     * Log a silly message
     */
    silly(...params: any[]) {
        if (this.logger) {
            this.logger.silly(params);
        }
    }

    /**
     * Shortcut to info
     */
    log(...params: any[]) {
        if (this.logger) {
            this.logger.log(params);
        }
    }
}

export const log = new Logger();
