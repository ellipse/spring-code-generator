import { TestBed } from '@angular/core/testing';

import { ElectronHttpClient } from './electron-http-client.service';

describe('ElectronHttpClientService', () => {
  let service: ElectronHttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ElectronHttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
