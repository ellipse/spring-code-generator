import { Injectable } from '@angular/core';
import { IpcClient } from '../core/IpcClient'
import { Observable } from 'rxjs'
import { Template } from '../../../app/template'
import { asyncChannels } from '../../../app/asyncChannels'
import { ModuleConfig } from '../../../app/java/module-config'
import { DbConfig } from '../../../app/db/db-config'

@Injectable({
    providedIn: 'root'
})
export class CodeService {
    constructor(private ipc: IpcClient) {
    }

    public getTemplates(): Observable<Template[]> {
        return this.ipc.sendAsync(asyncChannels.codeGeneratorTemplates)
    }

    public checkDb(config: DbConfig): Observable<string[]> {
        return this.ipc.sendAsync(asyncChannels.codeGeneratorCheckDb, config)
    }

    public checkModule(config: ModuleConfig): Observable<boolean> {
        return this.ipc.sendAsync(asyncChannels.codeGeneratorCheckModule, config)
    }

    public generateCode(template: Template, config: ModuleConfig): Observable<any> {
        return this.ipc.sendAsync(asyncChannels.codeGeneratorRun, template, config)
    }
}
