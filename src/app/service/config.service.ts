import { Injectable } from '@angular/core';
import { ModuleConfig } from '../../../app/java/module-config'

@Injectable({
    providedIn: 'root'
})
export class ConfigService implements ModuleConfig {
    // 数据库配置
    public host = 'localhost'
    public port = 3306
    public username = 'root'
    public password = '123456'
    public database: string
    public tableNamePrefix: string

    // 项目配置
    public projectRoot: string
    public moduleName: string
    public package: string
}
